package com.konami.axiom

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.LinearLayout

class ColumnLinearLayout(context: Context, attrs: AttributeSet, defStyle: Int): LinearLayout(context, attrs, defStyle) {
    constructor(context: Context, attributeSet: AttributeSet) : this(context, attributeSet, 0)

    private val TAG = "ColumnLinearLayout"

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        Log.e(TAG, "ColumnLinearLayout onLayout")
    }
}