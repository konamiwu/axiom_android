package com.konami.axiom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Typeface
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.view.children
import java.util.Random
import kotlin.math.ceil
import kotlin.math.max

class Column(private val context: Context): ViewGroup(context) {
    private val rnd = Random()
    private val buttonLayoutHeight: Int = resources.getDimension(R.dimen.button_height).toInt()
    private val padding = 1//resources.getDimension(R.dimen.border_width).toInt()
    private val borderWidth = context.resources.getDimension(R.dimen.border_width)
    private val grayPaint = Paint()
    private val borderPaint = Paint()
    private val buttonRect = Rect()
    private var highlight = false
    private var previousHighlightRow = 0
    private val confirmButton = LayoutInflater.from(context).inflate(R.layout.confirm_button, null)
    private val borderView = View(context)
    var row: Int = 0
        set(value) {
            field = value
            removeAllViews()
            repeat(row) {
                val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
                val textView = TextView(context)
                textView.textSize = resources.getDimension(R.dimen.cell_text_size)
                textView.setTypeface(textView.typeface, Typeface.BOLD)
                textView.gravity = Gravity.CENTER
                textView.setBackgroundColor(color)
                addView(textView)
            }

            confirmButton.findViewById<Button>(R.id.button).setOnClickListener {
                resetHighLight()
            }
            addView(confirmButton)
            addView(borderView)
        }

    init {
        setBackgroundColor(Color.WHITE)
        grayPaint.color = Color.DKGRAY
        borderPaint.color = context.getColor(R.color.highlight)
        borderPaint.style = Paint.Style.STROKE
        borderPaint.strokeWidth = context.resources.getDimension(R.dimen.border_width)
        borderView.background = resources.getDrawable(R.drawable.focus_border_background, null)
        borderView.visibility = GONE
    }
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        buttonRect.left = confirmButton.left
        buttonRect.top = confirmButton.top
        buttonRect.right = confirmButton.right
        buttonRect.bottom = confirmButton.bottom
        canvas.drawRect(buttonRect, grayPaint)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val height = (b - t - buttonLayoutHeight - padding * 2).toFloat() / row
        var y = padding.toFloat()
        val width = r - l
        var tempPadding = padding
        if (width - padding * 2 <= 0)
                tempPadding = 0
        (0..< children.count() - 2).forEach {
            getChildAt(it).layout(tempPadding, ceil(y).toInt(), ceil(r - l - padding.toFloat()).toInt(), ceil(y + height).toInt())
            y += height
        }
        confirmButton.layout(0, ceil(y).toInt(), (r - l), ceil(y).toInt() + buttonLayoutHeight)
        borderView.layout(0, 0, width, b)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val cellHeight = (height - buttonLayoutHeight - padding * 2).toFloat() / row
        children.forEach {
                it.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(cellHeight.toInt(), MeasureSpec.EXACTLY))
        }

        val spec = MeasureSpec.makeMeasureSpec(buttonLayoutHeight, MeasureSpec.EXACTLY)
        confirmButton.measure(widthMeasureSpec, spec)
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(width, height)
    }

    fun highLight(row: Int) {
        highlight = true
        previousHighlightRow = row
        borderView.visibility = VISIBLE
        confirmButton.findViewById<View>(R.id.button)?.background = context.getDrawable(R.drawable.confirm_button_background_focus)
        confirmButton.findViewById<Button>(R.id.button)?.setTextColor(Color.WHITE)
        (getChildAt(row) as? TextView)?.text = resources.getString(R.string.random)
        invalidate()
    }

    fun resetHighLight() {
        highlight = false
        borderView.visibility = GONE
        confirmButton.findViewById<Button>(R.id.button)?.background = context.getDrawable(R.drawable.confirm_button_background_normal)
        confirmButton.findViewById<Button>(R.id.button)?.setTextColor(Color.GRAY)
        (getChildAt(previousHighlightRow) as? TextView)?.text = null
        invalidate()
    }
}