package com.konami.axiom

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.activity.ComponentActivity
import androidx.core.view.children
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.Date
import java.util.Random
import java.util.Timer
import java.util.TimerTask

class MainActivity : ComponentActivity() {
    private lateinit var containerView: LinearLayout
    private lateinit var rowEditText: EditText
    private lateinit var columnEditText: EditText
    private lateinit var addButton: Button
    private var timer: Timer? = null
    private val timerStartDelay: Long = 1000
    private val timerPeriod: Long = 1000
    private val random = Random()
    private var row = 0
    private var column = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        containerView = findViewById(R.id.containerView)
        rowEditText = findViewById(R.id.rowEditText)
        columnEditText = findViewById(R.id.columnEditText)
        addButton = findViewById(R.id.addButton)

        addButton.setOnClickListener {
            val rowString = rowEditText.text.toString()
            val columnString = columnEditText.text.toString()
            if (rowString.isEmpty() || columnString.isEmpty())
                return@setOnClickListener

            row = rowEditText.text.toString().toInt()
            column = columnEditText.text.toString().toInt()
            containerView.removeAllViews()
            repeat(column) {
                addColumn(row)
            }
            startTimer()
        }
    }

    private fun addColumn(row: Int) {
        val column = Column(this)
        column.row = row
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT, 1f
        )
        containerView.addView(column, layoutParams)
    }

    private fun startTimer() {
        timer?.cancel()
        timer = null

        val timer = Timer()
        timer.schedule(object :TimerTask() { override fun run() {
            runOnUiThread {
                containerView.children.forEach {
                    (it as? Column)?.resetHighLight()
                }
                random.setSeed(Date().time)
                val highlightRow = random.nextInt(row)
                val highlightColumn = random.nextInt(column)
                (containerView.getChildAt(highlightColumn) as? Column)?.highLight(highlightRow)
            }
        }}, timerStartDelay, timerPeriod)
        this.timer = timer
    }
}